<?php

namespace Prodige\TestGenerator\Service;

use Twig\Environment;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GeneratorService
{
    private array $apiFieldNameResourceStructure = array(
        'sheetMetadata' => '/api/metadata_sheets',
        'lexLayerType' => '/api/lex_layer_types',
        'rubric' => '/api/rubrics',
        'profileCreator' => '/api/profiles',
        'profileEditor' => '/api/profiles',
        'profileAdmin' => '/api/profiles',
        'domain' => '/api/domains',
        'zoning' => '/api/zonings',
        'layer' => '/api/layers',
        'lexMapFormat' => '/api/lex_map_formats',
        'user' => '/api/users',
        'admin' => '/api/users',
        'lexSettingType' => '/api/lex_setting_types',
        'lexSettingCategory' => '/api/lex_setting_categories',
        'lexTemplateType' => '/api/lex_template_types',
        'area' => '/api/areas',
    );

    private $parameters;

    public function __construct(private Environment $twig, ParameterBagInterface $parameters,
    ) {
        $this->parameters = $parameters;
    }

    function findRecursiveRefValue($array, $fromFile = false, $useOfFileOrExport = false) {
        foreach ($array as $key => $value) {
//            if ($key == 'type' && $value == 'array' && !$fromFile) {
            if ($useOfFileOrExport === false && $key == 'type' && $value == 'array' && !$fromFile) {
                return null;
            }
            if ($key === '$ref') {
                return $value;
            }

            if (is_array($value)) {
                if ($useOfFileOrExport) {
                    $result = $this->findRecursiveRefValue($value, useOfFileOrExport: true);
                } else {
                    $result = $this->findRecursiveRefValue($value);
                }
                if ($result !== null) {
                    return $result;
                }
            }
        }

        return null;
    }

    function sortArrayByKeyRecursive(&$array) {
        ksort($array);
        foreach ($array as &$value) {
            if (is_array($value)) {
                $this->sortArrayByKeyRecursive($value);
            }
        }
    }


    public function generateCode(string $resource, string $url, string $pathToResource, array $exportSchema,
                                 array $allClassSchemas, $fileToCompare = null, bool $useOfFileOrExport = false) : void
    {

        //------------------------------------------------------------------------------------------------------------
        // find out routes and allowed methods
        //------------------------------------------------------------------------------------------------------------

        $allRoutes = $exportSchema['paths'];

        $allRoutesMethods = array();
        foreach ($allRoutes as $key => $value) {
            $allRoutesMethods[$key] = array_keys($value);
        }

        $correspondenceToSchemaFromFile = array();
        switch ($useOfFileOrExport) {
            case false:
                $allRoutesKeys = array_keys($exportSchema['paths']);
                break;

            case true:
                // /api/domains
                $resourceMethodsValues = $exportSchema['paths'][$url];

                // /api/domains/{id}
                $resourceIdMethodsValues = $exportSchema['paths'][$url . '/{id}'];

                $getOutputCollection = array_key_exists('get', $resourceMethodsValues)
                    ? $this->findRecursiveRefValue($resourceMethodsValues['get'], useOfFileOrExport: $useOfFileOrExport) : null;
                $getOutputItem = array_key_exists('get', $resourceIdMethodsValues)
                    ? $this->findRecursiveRefValue($resourceIdMethodsValues['get'], useOfFileOrExport: $useOfFileOrExport) : null;

                $postInputCollection = array_key_exists('post', $resourceMethodsValues)
                    ? $this->findRecursiveRefValue($resourceMethodsValues['post']['requestBody'], useOfFileOrExport: $useOfFileOrExport) : null;
                $postOutputCollection = array_key_exists('post', $resourceMethodsValues)
                    ? $this->findRecursiveRefValue($resourceMethodsValues['post'], useOfFileOrExport: $useOfFileOrExport) : null;

                $patchInputItem = array_key_exists('patch', $resourceIdMethodsValues)
                    ? $this->findRecursiveRefValue($resourceIdMethodsValues['patch']['requestBody'], useOfFileOrExport: $useOfFileOrExport) : null;
                $patchOutputItem = array_key_exists('patch', $resourceIdMethodsValues)
                    ? $this->findRecursiveRefValue($resourceIdMethodsValues['patch'], useOfFileOrExport: $useOfFileOrExport) : null;

                $schemasVariants = ['getOutputCollection', 'getOutputItem', 'postInputCollection',
                    'postOutputCollection', 'patchInputItem', 'patchOutputItem'];

                $allDefinitions = array();
                foreach ($schemasVariants as $variant) {
                    if (${$variant}) {
                        $schemaPath = explode('/', ${$variant});
                        $allDefinitions[$variant] = $exportSchema[$schemaPath[1]][$schemaPath[2]][$schemaPath[3]]; // export

                        if ($fileToCompare) {

                            $fileAllDefinitions[$variant] = $fileToCompare[$schemaPath[1]][$schemaPath[2]][$schemaPath[3]];
                            $fileVariant = $fileAllDefinitions[$variant];
                            $exportVariant = $allDefinitions[$variant];

                            $this->sortArrayByKeyRecursive($exportVariant);
                            $this->sortArrayByKeyRecursive($fileVariant);

                            $correspondenceToSchemaFromFile[$variant] = $exportVariant == $fileVariant;
                        }
                    }
                }

                break;
        }

        //------------------------------------------------------------------------------------------------------------
        // find out parameters for templates for each schema
        //------------------------------------------------------------------------------------------------------------
        $allPropertiesFormats = array();
        $allPropertiesTypes = array();
        $allPropertiesRefs = array();
        $allRequiredRefs = array();
        $allRequiredTypes = array();
        $allRequired = array();
        $allProperties = array();
        $allPropertiesNames = array();

        switch ($useOfFileOrExport) {
            case false:
                foreach ($allClassSchemas as $key => $value) {
                    $classStructure = $value;
                    $definitions = $classStructure['definitions'];
                    $keys = array_keys($definitions);
                    $firstKeyOfDefinitions = $keys[0];
                    $properties = $definitions[$firstKeyOfDefinitions]['properties'];
                    $propertiesNames = array_keys($properties);
                    $required = array_key_exists('required', $definitions[$firstKeyOfDefinitions])
                        ? $definitions[$firstKeyOfDefinitions]['required'] : array();
                }
                break;

            case true:

                foreach ($allDefinitions as $key => $value) {
                    $properties = $value['properties'];
                    $propertiesNames = array_keys($properties);
                    $required = array_key_exists('required', $value) ? $value['required'] : array();
                }
                break;

        }

        foreach ($allClassSchemas as $key => $value) {

            if ($useOfFileOrExport && !array_key_exists($key, $allDefinitions)) {
                continue;
            }

            switch ($useOfFileOrExport) {
                case false:
                    $classStructure = $value;
                    $definitions = $classStructure['definitions'];
                    $keys = array_keys($definitions);
                    $firstKeyOfDefinitions = $keys[0];
                    $properties = $definitions[$firstKeyOfDefinitions]['properties'];
                    $propertiesNames = array_keys($properties);
                    $required = array_key_exists('required', $definitions[$firstKeyOfDefinitions])
                        ? $definitions[$firstKeyOfDefinitions]['required'] : array();
                    break;

                case true:
                    $value = $allDefinitions[$key];
                    $properties = $value['properties'];
                    $propertiesNames = array_keys($properties);
                    $required = array_key_exists('required', $value) ? $value['required'] : array();
                    break;
            }

            $requiredRefs = array();
            $requiredTypes = array();
            $propertiesTypes = array();
            $propertiesRefs = array();
            $propertiesFormats = array();
            foreach ($propertiesNames as $propertyName) {

                if ($required && in_array($propertyName, $required)) {
                    $requiredTypes[$propertyName] = array_key_exists('type', $properties[$propertyName]) ? $properties[$propertyName]['type'] : null;
                    if ($this->findRecursiveRefValue($properties[$propertyName])) {
                        $requiredRefs[$propertyName] = $this->apiFieldNameResourceStructure[$propertyName];
                    }
                }

                $propertiesTypes[$propertyName] = array_key_exists('type', $properties[$propertyName]) ? $properties[$propertyName]['type'] : null;
                if ($this->findRecursiveRefValue($properties[$propertyName])) {
                    $propertiesRefs[$propertyName] = $this->apiFieldNameResourceStructure[$propertyName];
                }

                $propertiesFormats[$propertyName] = array_key_exists('format', $properties[$propertyName]) ? $properties[$propertyName]['format'] : null;
            }

            foreach ($propertiesFormats as $keyInner => $valueInner) {
                if ($valueInner == 'iri-reference') {
                    $propertiesRefs[$keyInner] = $this->apiFieldNameResourceStructure[$keyInner];
                    if (in_array($keyInner, $required)) {
                        $requiredRefs[$keyInner] = $this->apiFieldNameResourceStructure[$keyInner];
                    }
                }
            }

            $allPropertiesFormats[$key] = $propertiesFormats;
            $allPropertiesTypes[$key] = $propertiesTypes;
            $allPropertiesRefs[$key] = $propertiesRefs;
            $allRequiredRefs[$key] = $requiredRefs;
            $allRequiredTypes[$key] = $requiredTypes;
            $allRequired[$key] = $required;
            $allProperties[$key] = $properties;
            $allPropertiesNames[$key] = $propertiesNames;
        }

        $destFilePrefix = $this->parameters->get('test_generator')['dest_file_prefix'] ? $this->parameters->get('test_generator')['dest_file_prefix'] : 'Generated';

        //------------------------------------------------------------------------------------------------------------
        // pass parameters to twig templates
        //------------------------------------------------------------------------------------------------------------
        $template = 'base-test-generation.html.twig';
        $variables = [
            'resource' => $resource,
            'pathToResource' => $pathToResource,
            'url' => $url,
            'allFields' => $allPropertiesNames,
            'allRequired' => $allRequired,
            'allRequiredRefs' => $allRequiredRefs,
            'allRequiredTypes' => $allRequiredTypes,
            'allPropertiesTypes' => $allPropertiesTypes,
            'allPropertiesRefs' => $allPropertiesRefs,
            'allPropertiesFormats' => $allPropertiesFormats,
            'allRoutesMethods' => $allRoutesMethods,
            'correspondenceToSchemaFromFile' => $correspondenceToSchemaFromFile,
            'prefix' => $destFilePrefix,
        ];

        $code = $this->twig->render($template, $variables);
        $filesystem = new Filesystem();

        $destFilePath = $this->parameters->get('test_generator')['dest_file_path'] ? $this->parameters->get('test_generator')['dest_file_path'] : 'tests/generated/';
        $filesystem->dumpFile($destFilePath . $destFilePrefix . $resource . 'Test.php', $code);
    }
}