<?php

namespace Prodige\TestGenerator\Command;

use Prodige\TestGenerator\Service\GeneratorService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: 'admin:generate-tests',
    description: 'Generate tests using class name as input. You can also define the right API url for the class. Use option --url',
)]
class GeneratorCommand extends Command
{
    private $parameters;

    public function __construct(private KernelInterface $kernel, private GeneratorService $generatorService, ParameterBagInterface $parameters)
    {
        parent::__construct();
        $this->parameters = $parameters;
    }

    // php bin/console admin:g layer --url /api/layers
    protected function configure(): void
    {
        $this->addArgument('class',InputArgument::OPTIONAL, 'Class name')
            ->addOption('url', 'u', InputOption::VALUE_OPTIONAL, 'API url')
            ->addOption('export', 'exp', InputOption::VALUE_NONE, 'API Json')
            ->addOption('defaultPathToDocs', 'c', InputOption::VALUE_NONE, 'Path to documentation file from config')
            ->addOption('pathToDocs', 'p', InputOption::VALUE_OPTIONAL, 'Path to documentation file')
            ->addOption('defaultFolder', 'd', InputOption::VALUE_NONE, 'Path to folder with classes to test from config')
            ->addOption('folder', 'f', InputOption::VALUE_OPTIONAL, 'Path to folder with classes to test')
            ->addOption('filter', 't', InputOption::VALUE_OPTIONAL, 'Filter classes to test');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        //------------------------------------------------------------------------------------------------------------
        // get resource and url of the command
        //------------------------------------------------------------------------------------------------------------
        $resource = $input->getArgument('class'); // Layer
        $url = $input->getOption('url'); // /api/layers
        $export = $input->getOption('export'); // true
        $pathToDocs = $input->getOption('pathToDocs'); // documentation.json
        $defaultPathToDocs = $input->getOption('defaultPathToDocs'); // from generator.yaml
        $filter = $input->getOption('filter'); // lex
        $prefix = $this->parameters->get('test_generator')['dest_file_prefix'] ? $this->parameters->get('test_generator')['dest_file_prefix'] : 'Generated';

        if ($defaultPathToDocs) {
            $pathToDocs = $this->parameters->get('test_generator')['default_path_to_docs'];
        }

        $pathToFolder = null;
        if ($input->getOption('folder')) {
            $pathToFolder = $input->getOption('folder');
        } elseif ($input->getOption('defaultFolder')) {
            $pathToFolder = $this->parameters->get('test_generator')['default_folder'];
        }

        if ($filter && !$pathToFolder)
        {
            $io->error("If you want to filter the classes you need to provide the folder containing them.");
            return Command::FAILURE;
        }

        if (isset($pathToFolder) && $pathToFolder != 'recursive') {
            $io->writeln('Start of test generation for ' . $pathToFolder . ' folder.');
            $commandsOutput = '';
            $filesToTest = $this->findPhpFilesInFolder($pathToFolder);

            foreach ($filesToTest as $class) {
                if (stripos($class, $filter) === false) {
                    continue;
                }

                $recursiveCommand = new ArrayInput([
                    'command' => 'admin:generate-tests',
                    'class' => $class,
                    '--url' => $url,
                    '--export' => $export,
                    '--pathToDocs' => $pathToDocs,
                    '--folder' => 'recursive',
                ]);
                $bufferedOutput = new BufferedOutput();
                $application->run($recursiveCommand, $bufferedOutput);

                $outputContent = $bufferedOutput->fetch();
                $io->writeln($outputContent);
            }
            return Command::SUCCESS;
        }

        if (!$resource) {
            $io->error("Provide class name or define it in generator.yaml.");
            return Command::FAILURE;
        }

        //------------------------------------------------------------------------------------------------------------
        // command to get API schema with all information about all routes and classes
        //------------------------------------------------------------------------------------------------------------
        $schemaCommand = new ArrayInput([
            'command' => 'api:openapi:export',
        ]);

        $bufferedOutput = new BufferedOutput();
        $application->run($schemaCommand, $bufferedOutput);
        $schema = json_decode($bufferedOutput->fetch(), true);

        $allRoutes = $schema['paths'];

        $allRoutesResource = array();
        foreach ($allRoutes as $key => $value) {
            $operationId = array_values($value)[0]['operationId'];
            $re = '/[a-z]+([A-Za-z]+)(?:Item|Collection)/';

            if (preg_match($re, $operationId, $result)) {
                $allRoutesResource[$key] = $result[1];
            }
        }

        //------------------------------------------------------------------------------------------------------------
        // get url by class name
        //------------------------------------------------------------------------------------------------------------

        if ($url === null) {
            $url = array_search(strtolower($resource), array_map('strtolower', $allRoutesResource));
        }

        $resource = $allRoutesResource[$url];

        if (!$url || !$resource) {
            $io->error("Resource or url not found.");
            return Command::FAILURE;
        }

        //------------------------------------------------------------------------------------------------------------
        // command to get JSON of concrete schema depending on requests
        //------------------------------------------------------------------------------------------------------------

        $pathToResource = $this->findClassPath($resource);
        $pathToResource = str_replace('.php', '', $pathToResource); // App\.\.\Layer

        if (!$pathToResource) {
            $io->error("Class '{$resource}' not found.");
            return Command::FAILURE;
        }

        $getOutputCollection = new ArrayInput([
            'command' => 'api:json-schema:generate',
            'resource' => $pathToResource,
            '--collectionOperation' => 'get',
            '--type' => 'output',
        ]);

        $getOutputItem = new ArrayInput([
            'command' => 'api:json-schema:generate',
            'resource' => $pathToResource,
            '--itemOperation' => 'get',
            '--type' => 'output',
        ]);

        $postInputCollection = new ArrayInput([
            'command' => 'api:json-schema:generate',
            'resource' => $pathToResource,
            '--collectionOperation' => 'post',
            '--type' => 'input',
        ]);

        $postOutputCollection = new ArrayInput([
            'command' => 'api:json-schema:generate',
            'resource' => $pathToResource,
            '--collectionOperation' => 'post',
            '--type' => 'output',
        ]);

        $patchInputItem = new ArrayInput([
            'command' => 'api:json-schema:generate',
            'resource' => $pathToResource,
            '--itemOperation' => 'patch',
            '--type' => 'input',
        ]);

        $patchOutputItem = new ArrayInput([
            'command' => 'api:json-schema:generate',
            'resource' => $pathToResource,
            '--itemOperation' => 'patch',
            '--type' => 'output',
        ]);

        $schemasVariants = ['getOutputCollection', 'getOutputItem', 'postInputCollection',
            'postOutputCollection', 'patchInputItem', 'patchOutputItem'];


        foreach ($schemasVariants as $schemaVariant) {
            $bufferedOutput = new BufferedOutput();
            $application->run(${$schemaVariant}, $bufferedOutput);
            ${$schemaVariant . 'Array'} = json_decode($bufferedOutput->fetch(), true);
        }

        foreach ($schemasVariants as $variant) {
            $arrayName = $variant . 'Array';
            if (isset(${$arrayName})) {
                $allClassSchemas[$variant] = ${$arrayName};
            }
        }

        //------------------------------------------------------------------------------------------------------------
        // call generate service using documentation file content
        //------------------------------------------------------------------------------------------------------------

        if ($pathToDocs) {
            $fileContent = json_decode(file_get_contents($pathToDocs), true);

            $this->generatorService->generateCode($resource, $url, $pathToResource, $schema, $allClassSchemas,
                fileToCompare: $fileContent, useOfFileOrExport: true);
            $io->writeln("Tests for $resource class are generated using documentation file.
See $prefix$resource" . "Test.php");

            return Command::SUCCESS;

        }

        //------------------------------------------------------------------------------------------------------------
        // call generate service method using file schema
        //------------------------------------------------------------------------------------------------------------

        if ($export) {
            $this->generatorService->generateCode($resource, $url, $pathToResource, $schema, $allClassSchemas,
                useOfFileOrExport: true);

            $io->writeln("Tests for $resource class are generated using export of API schemas.
See $prefix$resource" . "Test.php");

            return Command::SUCCESS;
        }

        //------------------------------------------------------------------------------------------------------------
        // call generate service method using all class schemas
        //------------------------------------------------------------------------------------------------------------
        $this->generatorService->generateCode($resource, $url, $pathToResource, $schema, $allClassSchemas);
        $io->writeln("Tests for $resource class are generated using schemas of classes.
See $prefix$resource" . "Test.php");

        return Command::SUCCESS;
    }

    private function findClassPath(string $pathToResource): ?string
    {
        $finder = new Finder();
        $finder->files()->name($pathToResource . '.php')->in('src');

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $resourcePath = str_replace('/', '\\', $file->getRelativePathname());
            return 'App\\' . $resourcePath;
        }

        return null;
    }

    function findPhpFilesInFolder(string $folderPath): array
    {
        $finder = new Finder();
        $finder->files()->in($folderPath)->name('*.php');

        $fileNames = [];

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $fileNames[] = $file->getBasename('.php');
        }

        return $fileNames;
    }

}