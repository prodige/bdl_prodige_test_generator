<?php

namespace Prodige\TestGenerator\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('test_generator');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
            ->scalarNode('default_path_to_docs')->defaultValue('%kernel.project_dir%/documentation.json')->end()
            ->scalarNode('dest_file_path')->defaultValue('tests/generated/')->end()
            ->scalarNode('dest_file_prefix')->defaultValue('Super')->end()
            ->scalarNode('default_folder')->defaultValue('%kernel.project_dir%/src/Entity')->end()
            ->scalarNode('api_login')->defaultValue('admin@prodige.fr')->end()
            ->scalarNode('api_password')->defaultValue('test')->end()
            ->end();

        return $treeBuilder;
    }
}
