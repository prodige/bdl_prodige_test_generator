<?php

namespace Prodige\TestGenerator;

use Prodige\TestGenerator\Command\GeneratorCommand;
use Prodige\TestGenerator\Service\GeneratorService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class TestGeneratorBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->register(GeneratorService::class)
            ->setAutowired(true)
            ->setAutoconfigured(true);

        // Register the Environment service
        $container->register(Environment::class, Environment::class)
            ->setAutowired(true)
            ->setAutoconfigured(true);

        // Create a custom Twig loader and add your template path
        $container->register('prodige_test_generator.twig_loader', FilesystemLoader::class)
            ->addArgument([
                '%kernel.project_dir%/vendor/prodige/test-generator/Template' => 'vendor/prodige/test-generator/Template',
            ]);


        // Alias the Twig loader interface to your custom loader service
        $container->setAlias('Twig\Loader\LoaderInterface', 'prodige_test_generator.twig_loader');

        // Configure Twig to use the custom loader
        $container->getDefinition(Environment::class)
            ->addMethodCall('setLoader', [$container->getDefinition('prodige_test_generator.twig_loader')]);

        $container->register(GeneratorCommand::class)
            ->addTag('console.command')
            ->setAutowired(true)
            ->setAutoconfigured(true);
    }

    public function getContainerExtension(): ExtensionInterface
    {
        return new DependencyInjection\TestGeneratorExtension();
    }

    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

}