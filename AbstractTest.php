<?php

namespace Prodige\TestGenerator;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use http\Env\Response;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class AbstractTest extends ApiTestCase
{
    private ?string $token = null;
    private Client $clientWithCredentials;

//    use RefreshDatabaseTrait;

    public function setUp(): void
    {
        self::bootKernel();
    }


    protected function createClientWithCredentials(string $url, string $method = 'GET', $headers = [], $body = [], $token = null): ResponseInterface
    {
        $token = $token ?: $this->getToken();

        $client = static::createClient([], [
            'headers' => [
                'authorization' => 'Bearer ' . $token,
                'Accept' => 'application/ld+json'
            ]
        ]);


        return $client->request($method, $url, [
                'headers' => $headers,
                'json' => $body,
            ]
        );
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken($body = []): string
    {
        if (!is_null($this->token)) {
            return $this->token;
        }

        $response = static::createClient()->request('POST', '/api/login', [
            'headers' => [
                'Content-Type' => 'application/ld+json'
            ],
            'json' => $body ?: [
                'login' => 'admin@prodige.fr',
                'password' => 'test',
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->access_token;

        return $data->access_token;
    }

    public function localAssertArrayHasKeys(array $array, array $keys): void
    {
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }

    public function localAssertGetError(int $code = 404, string $title = 'An error occurred', string $description = 'Not Found'): void
    {
        $this->assertResponseStatusCodeSame($code);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertJsonContains([
            'hydra:title' => $title,
            'hydra:description' => $description,
        ]);
    }


    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}