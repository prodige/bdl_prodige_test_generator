# Générateur de tests
## 1. Installation
Pour installer test-generator, entrez la commande dans votre projet :

`composer require prodige/test-generator:[version ou branche p.ex. dev-develop]`

Le code du générateur de tests est maintenant situé dans le dossier `vendor`.

___

## 2. Mise en place

### 2.1.
Dans le dossier `config/packages`, placez le fichier de configuration `generator.yaml`, qui contient les paramètres par défaut du générateur de tests.

### 2.2.
Entrez la commande :

`php bin/console cache:clear`

___

## 3. Utilisation

Vous pouvez générer des tests pour une classe spécifique en utilisant la commande :

`php bin/console admin:generate-tests nomDeLaClass`

___

## 4. Comment ça marche

Lorsque vous entrez une commande pour générer des tests, il se passe ce qui suit :
1. La commande class génère un schéma Json pour la classe sélectionnée (ou plusieurs classes)
2. Le schéma Json est envoyé au service
3. Le service récupère les paramètres nécessaires du schéma, tels que les noms des champs, les types de données, la présence et le nombre de champs obligatoires, etc
5. Le service transmet les paramètres au modèle Twig de base, qui comprend également tous les modèles de test
5. Les modèles intègrent les paramètres dans le code des futurs tests
6. Le service rend les modèles et enregistre le code fini dans le dossier souhaité
7. Lorsque les tests sont générés, le développeur peut les exécuter à l'aide de la commande `php bin/phpunit pathToTestClass`

___

## 5. Options

La commande de génération de tests sans options se présente comme suit :

`php bin/console admin:generate-tests nomDeLaClass`

Dans ce cas, le schéma Json est généré en utilisant la commande `api:json-schema:generate`.
En outre, vous pouvez également utiliser différentes options de commande.

### 5.1.

Pour définir l'url qui est utilisée dans l'API de la classe :

`php bin/console admin:generate-tests -u /api/url nomDeLaClass`

p.ex.
`php bin/console admin:generate-tests -u /api/domains domain`

### 5.2.
Pour appliquer la génération de schéma Json en utilisant la commande `api:openapi:export` :

`php bin/console admin:generate-tests -exp nomDeLaClass`

### 5.3.
Pour comparer le schéma de classe généré et le schéma du fichier de documentation de l'API dans un test :

`php bin/console admin:generate-tests -c nomDeLaClass`

Dans ce cas, le chemin vers le fichier de documentation est pris dans le fichier de configuration `generator.yaml`.

### 5.5.
Pour comparer le schéma de classe généré et le schéma du fichier de documentation de l'API dans un test :

`php bin/console admin:generate-tests -p chemin/vers/le/fichier/de/documentation nomDeLaClass`

p.ex.
`php bin/console admin:generate-tests -p documentation.json domain`


Dans ce cas, le chemin vers le fichier de documentation est saisi manuellement.

### 5.5.
Pour spécifier le dossier pour les classes duquel vous voulez générer des tests :

`php bin/console admin:generate-tests -d`

Dans ce cas, le chemin vers le dossier est pris dans le fichier de configuration `generator.yaml`.

### 5.6.
Pour spécifier le dossier pour les classes duquel vous voulez générer des tests :

`php bin/console admin:generate-tests -f chemin/vers/le/dossier`

p.ex.
`php bin/console admin:generate-tests -f cd src/Entity/Administration/`

Dans ce cas, le chemin vers le dossier est saisi manuellement.

### 5.7.
Pour filtrer les classes pour la génération de tests en fonction de leur nom :

`php bin/console admin:generate-tests -d -t filter`

p.ex.
`php bin/console admin:generate-tests -d -t lex`

Pour effectuer le filtrage, il est nécessaire de spécifier le dossier d'où sont extraits les fichiers à tester.

___

## 5. Paramètres du générateur de test

Dans le fichier `generator.yaml`, vous pouvez modifier les paramètres du générateur de test que vous souhaitez définir par défaut.

### 5.1.

Chemin du fichier de documentation de l'API :

`default_path_to_docs`

### 5.2.

Chemin d'accès au dossier dans lequel les fichiers de test générés seront enregistrés :

`dest_file_path`

### 5.3.

Préfixe du nom de fichier généré :

`dest_file_prefix`

### 5.5.

Dossier pour les classes duquel vous voulez générer des tests :

`default_folder`

___

*Bon développement :)*






